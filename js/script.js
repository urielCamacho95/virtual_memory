
//JQuery code here!!
//F.C.C. BUAP

//Empty matrix
var matrix_A = [];
for(var i=0; i<3; i++) {
    matrix_A[i] = new Array(3);
}
//var matrix_B=matrix_result=matrix_A;
var matrix_B = [];
for(var i=0; i<3; i++) {
    matrix_B[i] = new Array(3);
}
//Matrix result
var matrix_result = [];
for(var i=0; i<3; i++) {
    matrix_result[i] = new Array(3);
}
for(var i=0;i<3;i++){
	for(var j=0;j<3;j++){
		matrix_result[i][j]=0;
	}
}

//Physical memory
var physical_memory = [false,false];

//H.D.
var hd = [$('tbody tr#1'),$('tbody tr#1'),
$('tbody tr#3'),$('tbody tr#4'),$('tbody tr#5'),$('tbody tr#6'),$('tbody tr#7'),$('tbody tr#8'),
$('tbody tr#9'),$('tbody tr#10'),$('tbody tr#11'),$('tbody tr#12'),$('tbody tr#13'),$('tbody tr#14'),
$('tbody tr#15'),$('tbody tr#16'),$('tbody tr#17'),$('tbody tr#18'),$('tbody tr#19'),$('tbody tr#20'),
$('tbody tr#21'),$('tbody tr#22'),$('tbody tr#23'),$('tbody tr#24'),$('tbody tr#25'),$('tbody tr#26'),
$('tbody tr#27'),$('tbody tr#28'),$('tbody tr#29'),$('tbody tr#30'),$('tbody tr#31'),$('tbody tr#32'),
$('tbody tr#33'),$('tbody tr#34'),$('tbody tr#35'),$('tbody tr#36'),$('tbody tr#37'),$('tbody tr#38'),
$('tbody tr#39'),$('tbody tr#40'),$('tbody tr#41'),$('tbody tr#42'),$('tbody tr#43'),$('tbody tr#44'),
$('tbody tr#45'),$('tbody tr#46'),$('tbody tr#47'),$('tbody tr#48'),$('tbody tr#49'),$('tbody tr#50')];


//Function to search into physical memory or load it from table's page
function search($obj){
	$('.textbox textarea').text($('.textbox textarea').text()+"\nArchivo a ejecutar: "+$obj.find('.methods_name').text());
	var i =0;
	var flag = false;
	//Check if there's space in physical memory or free one space and add it from H.D. to Virtual memory
	for (i = 0; i < physical_memory.length; i++){
		//We just turn on the bit of lee_Matrix_A function in our table's page and turn on physical memory
		if(physical_memory[i]==false){
			//alert('entra');
			physical_memory[i]=$obj;
			//alert($obj.find('.methods_name').text());
			$('.physical_memory').children('label:eq('+i+')').text($obj.find('.methods_name').text());
			$obj.find('.bit').text('1');
			flag = true;
			return flag;
		}
	}
	//If isn't available space, we need remove one process from physical memory and
	//disable his corresponding bit flag, and finally restart our previous operation
	//alert(physical_memory[0].find('.number').text());
	$('.textbox textarea').text($('.textbox textarea').text()+"\nOcurrio un fallo de pagina... cargando desde HD a memoria fisica.");
	physical_memory[0].find('.bit').text('0');
	physical_memory[0] = physical_memory[1];
	$('.physical_memory').children('label:eq(0)').text(physical_memory[0].find('.methods_name').text());
	physical_memory[1]=false;
	return false;
	//$('tbody tr').find('number:eq('+(parseInt(physical_memory[0].text())-1)+')').parent().children('.bit').text('0');
	//physical_memory.splice(0,0,physical_memory[1]);

}
//reading matrix A Function
function lee_Matrix_A($obj){
	var flag = search($obj);
	if(!flag)flag=search($obj);
	//writing into textarea what exactly the process does
	$('.textbox textarea').text($('.textbox textarea').text()+"Ejecutando 'leyendo de la matrix A'...");
	matrix_A[0].splice(0,0,$('.matrix_a .first_row input.first_column').val());
	matrix_A[0].splice(1,0,$('.matrix_a .first_row input.second_column').val());
	matrix_A[0].splice(2,0,$('.matrix_a .first_row input.third_column').val());
	matrix_A[1].splice(0,0,$('.matrix_a .second_row input.first_column').val());
	matrix_A[1].splice(1,0,$('.matrix_a .second_row input.second_column').val());
	matrix_A[1].splice(2,0,$('.matrix_a .second_row input.third_column').val());
	matrix_A[2].splice(0,0,$('.matrix_a .third_row input.first_column').val());
	matrix_A[2].splice(1,0,$('.matrix_a .third_row input.second_column').val());
	matrix_A[2].splice(2,0,$('.matrix_a .third_row input.third_column').val());
	//cleaning matrix_a input's
	$('.matrix_a').find('input').val('');

}

//reading matrix B Function
function lee_Matrix_B($obj){
	var flag=search($obj)
	if(!flag)flag=search($obj);
	//writing into textarea what exactly the process does
	$('.textbox textarea').text($('.textbox textarea').text()+"\nEjecutando 'leyendo de la matrix B'...");
	matrix_B[0].splice(0,0,$('.matrix_b .first_row input.first_column').val());
	matrix_B[0].splice(1,0,$('.matrix_b .first_row input.second_column').val());
	matrix_B[0].splice(2,0,$('.matrix_b .first_row input.third_column').val());
	matrix_B[1].splice(0,0,$('.matrix_b .second_row input.first_column').val());
	matrix_B[1].splice(1,0,$('.matrix_b .second_row input.second_column').val());
	matrix_B[1].splice(2,0,$('.matrix_b .second_row input.third_column').val());
	matrix_B[2].splice(0,0,$('.matrix_b .third_row input.first_column').val());
	matrix_B[2].splice(1,0,$('.matrix_b .third_row input.second_column').val());
	matrix_B[2].splice(2,0,$('.matrix_b .third_row input.third_column').val());
	//cleaning matrix_a input's
	$('.matrix_b').find('input').val('');
} 
//Writing matrix A into inputs
function escribe_Matrix_A($obj){
	var flag=search($obj);
	if(!flag) flag=search($obj);
	//writing into textarea what exactly the process does
	$('.textbox textarea').text($('.textbox textarea').text()+"\nEjecutando 'escribiendo a la matrix A'...");
	$('.matrix_a .first_row input.first_column').val(matrix_A[0][0]);
	$('.matrix_a .first_row input.second_column').val(matrix_A[0][1]);
	$('.matrix_a .first_row input.third_column').val(matrix_A[0][2]);
	$('.matrix_a .second_row input.first_column').val(matrix_A[1][0]);
	$('.matrix_a .second_row input.second_column').val(matrix_A[1][1]);
	$('.matrix_a .second_row input.third_column').val(matrix_A[1][2]);
	$('.matrix_a .third_row input.first_column').val(matrix_A[2][0]);
	$('.matrix_a .third_row input.second_column').val(matrix_A[2][1]);
	$('.matrix_a .third_row input.third_column').val(matrix_A[2][2]);
}
//Writing matrix B into inputs
function escribe_Matrix_B($obj){
	//writing into textarea what exactly the process does
	var flag=search($obj);
	if(!flag)flag=search($obj);
	$('.textbox textarea').text($('.textbox textarea').text()+"\nEjecutando 'Escribiendo a la matrix B'...");
	$('.matrix_b .first_row input.first_column').val(matrix_B[0][0]);
	$('.matrix_b .first_row input.second_column').val(matrix_B[0][1]);
	$('.matrix_b .first_row input.third_column').val(matrix_B[0][2]);
	$('.matrix_b .second_row input.first_column').val(matrix_B[1][0]);
	$('.matrix_b .second_row input.second_column').val(matrix_B[1][1]);
	$('.matrix_b .second_row input.third_column').val(matrix_B[1][2]);
	$('.matrix_b .third_row input.first_column').val(matrix_B[2][0]);
	$('.matrix_b .third_row input.second_column').val(matrix_B[2][1]);
	$('.matrix_b .third_row input.third_column').val(matrix_B[2][2]);
}
function producto(x,y,str,$obj){
	var flag=search($obj);
	if(!flag)flag=search($obj);
	$('.textbox textarea').text($('.textbox textarea').text()+'\n'+str);
	return x*y;
}
function sumaxyz(x,y,z,str,$obj){
	var flag=search($obj);
	if(!flag)flag=search($obj);
	$('.textbox textarea').text($('.textbox textarea').text()+'\n'+str);
	return ((x+y)+z);
}
function guarda_r(x,y,value,str,$obj){
	var flag=search($obj);
	if(!flag)flag=search($obj);
	matrix_result[x][y] = value;
	$('.textbox textarea').text($('.textbox textarea').text()+'\n'+str);
}

function escribe_r($obj){
	var flag=search($obj);
	if(!flag)flag=search($obj);
	//writing into textarea what exactly the process does
	$('.textbox textarea').text($('.textbox textarea').text()+"\nEjecutando 'escribiendo a la matrix resultado'...");
	$('.matrix_result .first_row input.first_column').val(matrix_result[0][0]);
	$('.matrix_result .first_row input.second_column').val(matrix_result[0][1]);
	$('.matrix_result .first_row input.third_column').val(matrix_result[0][2]);
	$('.matrix_result .second_row input.first_column').val(matrix_result[1][0]);
	$('.matrix_result .second_row input.second_column').val(matrix_result[1][1]);
	$('.matrix_result .second_row input.third_column').val(matrix_result[1][2]);
	$('.matrix_result .third_row input.first_column').val(matrix_result[2][0]);
	$('.matrix_result .third_row input.second_column').val(matrix_result[2][1]);
	$('.matrix_result .third_row input.third_column').val(matrix_result[2][2]);
}

function play(){
	var x1=x2=x3=suma=0;
	lee_Matrix_A($('tr#1'));
	lee_Matrix_B($('tr#2'));

	escribe_Matrix_A($('tr#3'));
	escribe_Matrix_B($('tr#4'));
	//0,0
	x1 = producto(matrix_A[0][0],matrix_B[0][0],"Ejecutando 'producto x1 A(1,1) B(1,1)'",$('tr#5'));
	x2 = producto(matrix_A[0][1],matrix_B[1][0],"Ejecutando 'producto x1 A(1,2) B(2,1)'",$('tr#6'));
	x3 = producto(matrix_A[0][2],matrix_B[2][0],"Ejecutando 'producto x1 A(1,3) B(3,1)'",$('tr#7'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3, $('tr#8'));
	guarda_r(0,0,suma,"Ejecutando 'Guardando suma en R(1,1)'"+suma, $('tr#9'));
	//0,1
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[0][0],matrix_B[0][1],"Ejecutando 'producto x1 A(1,1) B(1,2)'",$('tr#10'));
	x2 = producto(matrix_A[0][1],matrix_B[1][1],"Ejecutando 'producto x1 A(1,2) B(2,2)'",$('tr#11'));
	x3 = producto(matrix_A[0][2],matrix_B[2][1],"Ejecutando 'producto x1 A(1,3) B(3,2)'",$('tr#12'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#13'));
	guarda_r(0,1,suma,"Ejecutando 'Guardando suma en R(1,2)'"+suma,$('tr#14'));
	//0,2
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[0][0],matrix_B[0][2],"Ejecutando 'producto x1 A(1,1) B(1,3)'",$('tr#15'));
	x2 = producto(matrix_A[0][1],matrix_B[1][2],"Ejecutando 'producto x1 A(1,2) B(2,3)'",$('tr#16'));
	x3 = producto(matrix_A[0][2],matrix_B[2][2],"Ejecutando 'producto x1 A(1,3) B(3,3)'",$('tr#17'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#18'));
	guarda_r(0,2,suma,"Ejecutando 'Guardando suma en R(1,3)'",$('tr#19'));
	//1,0
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[1][0],matrix_B[0][0],"Ejecutando 'producto x1 A(2,1) B(1,1)'",$('tr#20'));
	x2 = producto(matrix_A[1][1],matrix_B[1][0],"Ejecutando 'producto x1 A(2,2) B(2,1)'",$('tr#21'));
	x3 = producto(matrix_A[1][2],matrix_B[2][0],"Ejecutando 'producto x1 A(2,3) B(3,1)'",$('tr#22'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#23'));
	guarda_r(1,0,suma,"Ejecutando 'Guardando suma en R(2,1)'",$('tr#24'));
	//1,1
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[1][0],matrix_B[0][1],"Ejecutando 'producto x1 A(2,1) B(1,2)'",$('tr#25'));
	x2 = producto(matrix_A[1][1],matrix_B[1][1],"Ejecutando 'producto x1 A(2,2) B(2,2)'",$('tr#26'));
	x3 = producto(matrix_A[1][2],matrix_B[2][1],"Ejecutando 'producto x1 A(2,3) B(3,2)'",$('tr#27'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#28'));
	guarda_r(1,1,suma,"Ejecutando 'Guardando suma en R(2,2)'",$('tr#29'));
	//1,2
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[1][0],matrix_B[0][2],"Ejecutando 'producto x1 A(2,1) B(1,3)'",$('tr#30'));
	x2 = producto(matrix_A[1][1],matrix_B[1][2],"Ejecutando 'producto x1 A(2,2) B(2,3)'",$('tr#31'));
	x3 = producto(matrix_A[1][2],matrix_B[2][2],"Ejecutando 'producto x1 A(2,3) B(3,3)'",$('tr#32'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#33'));
	guarda_r(1,2,suma,"Ejecutando 'Guardando suma en R(2,3)'",$('tr#34'));
	//2,0
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[2][0],matrix_B[0][0],"Ejecutando 'producto x1 A(3,1) B(1,1)'",$('tr#35'));
	x2 = producto(matrix_A[2][1],matrix_B[1][0],"Ejecutando 'producto x1 A(3,2) B(2,1)'",$('tr#36'));
	x3 = producto(matrix_A[2][2],matrix_B[2][0],"Ejecutando 'producto x1 A(3,3) B(3,1)'",$('tr#37'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#38'));
	guarda_r(2,0,suma,"Ejecutando 'Guardando suma en R(3,1)'",$('tr#39'));
	//2,1
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[2][0],matrix_B[0][1],"Ejecutando 'producto x1 A(3,1) B(1,2)'",$('tr#40'));
	x2 = producto(matrix_A[2][1],matrix_B[1][1],"Ejecutando 'producto x1 A(3,2) B(2,2)'",$('tr#41'));
	x3 = producto(matrix_A[2][2],matrix_B[2][1],"Ejecutando 'producto x1 A(3,3) B(3,2)'",$('tr#42'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#43'));
	guarda_r(2,1,suma,"Ejecutando 'Guardando suma en R(3,2)'",$('tr#44'));
	//2,2
	x1=x2=x3=suma=0;
	x1 = producto(matrix_A[2][0],matrix_B[0][2],"Ejecutando 'producto x1 A(2,1) B(1,3)'",$('tr#45'));
	x2 = producto(matrix_A[2][1],matrix_B[1][2],"Ejecutando 'producto x1 A(2,2) B(2,3)'",$('tr#46'));
	x3 = producto(matrix_A[2][2],matrix_B[2][2],"Ejecutando 'producto x1 A(2,3) B(3,3)'",$('tr#47'));

	suma = sumaxyz(x1,x2,x3,"Ejecutando 'suma: "+x1+" + "+x2+" + "+x3,$('tr#48'));
	guarda_r(2,2,suma,"Ejecutando 'Guardando suma en R(3,3)'",$('tr#49'));


	escribe_r($('tr#50'));
}

$(document).ready(function(){
	
	//alert(x+','+y+','+z);
	$('.preloader').css({display:"none"});

	$('#play').click(play);
	
	//alert('Hola');
	/*setTimeout(function(){
		$('.preloader').css({display:"none"});	
	},2000);*/


	
	
	//alert('Hola');
});